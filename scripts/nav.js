(($) => {
    $(document).ready(() => {
        const $ul = $('.nav__list');

        // controls if nav is displayed when switching beetwing different breakpoints
        $(window).on('resize', () => {
            if (window.innerWidth > breakpoints.lg) {
                $ul.fadeIn();
            }
        })
        
        // show/hide nav
        $('.nav__burger-nav').on('click', ({ target: t }) => {
            $ul.slideToggle('fast');
        })

        // close nav only if it's a burger one
        $('.nav__list-item').on('click', () => {
            if (window.innerWidth < breakpoints.lg) {
                $ul.slideToggle('fast');            
            }
        })
    })
})(jQuery)