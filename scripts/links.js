(($) => {
    $(document).ready(() => {
        $('.nav__list-item a').on('click', e => {
            console.log('click')
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $($(e.target).attr('href')).offset().top - 100
            }, 600);
        })
    })
})(jQuery)