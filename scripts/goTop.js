(($) => {
    const $goTopButton = $('.nav__go-top');
    $(window).on('scroll', e => {
        if (window.scrollY > 450) {
            return $goTopButton.fadeIn('slow')
        }

        return $goTopButton.fadeOut('fast');
    })
    $goTopButton.on('click', () => {
        $('body, html').animate({
            scrollTop: 0,
        }, 400)
    })
})(jQuery)