(($) => {
    $(document).ready(() => {
        const $works = $('.works-list__item');

        $works.on('mouseenter', ({ target: t }) => {
            const eventTarget = $(t);
            eventTarget.children().fadeIn('slow');

            // subscribing on the current event causer
            eventTarget.on('mouseleave', () => {
                eventTarget.children().fadeOut('slow');
            })
        })

    })
})(jQuery)
