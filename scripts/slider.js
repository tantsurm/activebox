(($) => {
    const $slides = $('.slide');
    const $controls = $('.control');
    const $descriptions = $('.description');
    const slidesAmount = $slides.length;

    let sliderCurrentId = 0;

    setInterval(() => {
        if (sliderCurrentId === slidesAmount - 1) {
            $($slides[sliderCurrentId]).toggleClass('active');
            $($controls[sliderCurrentId]).toggleClass('active');
            $($descriptions[sliderCurrentId]).toggleClass('active');

            sliderCurrentId = 0
        }

        $($slides[sliderCurrentId]).toggleClass('active')
        $($controls[sliderCurrentId]).toggleClass('active');
        $($descriptions[sliderCurrentId]).toggleClass('active');


        $($slides[sliderCurrentId + 1]).toggleClass('active');
        $($controls[sliderCurrentId + 1]).toggleClass('active')
        $($descriptions[sliderCurrentId + 1]).toggleClass('active');
    }, 5000)

})(jQuery)